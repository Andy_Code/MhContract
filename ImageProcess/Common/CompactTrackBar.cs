﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Windows.Forms;

namespace Common
{
    public class CompactTrackBar : TrackBar
    {
        public CompactTrackBar()
        {
            
            TickStyle = TickStyle.None;
        }

        protected internal virtual bool ShowFocusCue
        {
            get
            {
                return false;
            }
        }
    }
}
