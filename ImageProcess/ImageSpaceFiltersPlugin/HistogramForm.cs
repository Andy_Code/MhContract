﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;

namespace ImageSpaceFiltersPlugin
{
    public partial class HistogramForm : Form
    {
        public HistogramForm()
        {
            InitializeComponent();
        }

        Histogram histogram = null;

        public void SetHistogram(Histogram _histogram)
        {
            histogram = _histogram;
            UpdateHistogramBitmap();
        }

        Bitmap hR = null, hG = null, hB = null;

        const int Gap = 10;

        private void UpdateHistogramBitmap()
        {
            if (histogram != null)
            {
                hR = histogram.DrawHistogram(histogramPanel.ClientRectangle.Width,
                    histogramPanel.ClientRectangle.Height / 3 - Gap, Color.Red, Histogram.HistogramChannel.R);
                hG = histogram.DrawHistogram(histogramPanel.ClientRectangle.Width,
                    histogramPanel.ClientRectangle.Height / 3 - Gap, Color.Green, Histogram.HistogramChannel.G);
                hB = histogram.DrawHistogram(histogramPanel.ClientRectangle.Width,
                    histogramPanel.ClientRectangle.Height / 3 - Gap, Color.Blue, Histogram.HistogramChannel.B);
            }
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void histogramPanel_Resize(object sender, EventArgs e)
        {
            UpdateHistogramBitmap();
            Refresh();
        }

        private void histogramPanel_Paint(object sender, PaintEventArgs e)
        {
            int top = 0;
            if (hR != null && hB != null && hG != null)
            {
                e.Graphics.DrawImage(hR, 0, top);
                e.Graphics.DrawLine(Pens.Black, 0, top + hR.Height, hR.Width, top + hR.Height);
                e.Graphics.DrawLine(Pens.Black, 0, top, 0, top + hR.Height);
                top += hR.Height + Gap;
                e.Graphics.DrawImage(hG, 0, top);
                e.Graphics.DrawLine(Pens.Black, 0, top + hG.Height, hG.Width, top + hG.Height);
                e.Graphics.DrawLine(Pens.Black, 0, top, 0, top + hG.Height);
                top += hG.Height + Gap;
                e.Graphics.DrawImage(hB, 0, top);
                e.Graphics.DrawLine(Pens.Black, 0, top + hB.Height, hB.Width, top + hB.Height);
                e.Graphics.DrawLine(Pens.Black, 0, top, 0, top + hB.Height);
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public delegate void NotifyEvent();
        public event NotifyEvent HistogramEqualize;

        private void histogramEqualizationButton_Click(object sender, EventArgs e)
        {
            if (HistogramEqualize != null)
                HistogramEqualize();
            UpdateHistogramBitmap();
            Refresh();
        }
    }
}
