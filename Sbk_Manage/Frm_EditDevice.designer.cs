﻿namespace Sbk_Manage
{
    partial class Frm_EditDevice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_EditDevice));
            this.label1 = new System.Windows.Forms.Label();
            this.txtSbmc = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtXhgg = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCcbh = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSbtm = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSbpp = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtZczh = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSyks = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtGhs = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtGhslxfs = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtCj = new System.Windows.Forms.TextBox();
            this.txtCjlxfs = new System.Windows.Forms.TextBox();
            this.dtpYsrq = new System.Windows.Forms.DateTimePicker();
            this.dtpRzrq = new System.Windows.Forms.DateTimePicker();
            this.chkYsrq = new System.Windows.Forms.CheckBox();
            this.chkRzrq = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtZbq = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtSbsl = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtSbdj = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtSbbzsm = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cmbSblb = new System.Windows.Forms.ComboBox();
            this.chkTf = new System.Windows.Forms.CheckBox();
            this.chkJk = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtWmgsmc = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtBgd = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtJyjyzm = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.dtpjyrq = new System.Windows.Forms.DateTimePicker();
            this.label21 = new System.Windows.Forms.Label();
            this.textSxzz = new System.Windows.Forms.TextBox();
            this.txtBfwh = new System.Windows.Forms.TextBox();
            this.chkBfwh = new System.Windows.Forms.CheckBox();
            this.label22 = new System.Windows.Forms.Label();
            this.dtpBfrq = new System.Windows.Forms.DateTimePicker();
            this.label23 = new System.Windows.Forms.Label();
            this.txtHtbh = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "设备名称：";
            // 
            // txtSbmc
            // 
            this.txtSbmc.Location = new System.Drawing.Point(141, 38);
            this.txtSbmc.Name = "txtSbmc";
            this.txtSbmc.Size = new System.Drawing.Size(124, 26);
            this.txtSbmc.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(290, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "型号规格：";
            // 
            // txtXhgg
            // 
            this.txtXhgg.Location = new System.Drawing.Point(384, 38);
            this.txtXhgg.Name = "txtXhgg";
            this.txtXhgg.Size = new System.Drawing.Size(124, 26);
            this.txtXhgg.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(47, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "出厂编号：";
            // 
            // txtCcbh
            // 
            this.txtCcbh.Location = new System.Drawing.Point(141, 70);
            this.txtCcbh.Name = "txtCcbh";
            this.txtCcbh.Size = new System.Drawing.Size(124, 26);
            this.txtCcbh.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(290, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "设备条码：";
            // 
            // txtSbtm
            // 
            this.txtSbtm.Location = new System.Drawing.Point(384, 70);
            this.txtSbtm.Name = "txtSbtm";
            this.txtSbtm.Size = new System.Drawing.Size(124, 26);
            this.txtSbtm.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(47, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "设备品牌：";
            // 
            // txtSbpp
            // 
            this.txtSbpp.Location = new System.Drawing.Point(141, 101);
            this.txtSbpp.Name = "txtSbpp";
            this.txtSbpp.Size = new System.Drawing.Size(124, 26);
            this.txtSbpp.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(290, 106);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "注册证号：";
            // 
            // txtZczh
            // 
            this.txtZczh.Location = new System.Drawing.Point(384, 101);
            this.txtZczh.Name = "txtZczh";
            this.txtZczh.Size = new System.Drawing.Size(124, 26);
            this.txtZczh.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(47, 134);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 16);
            this.label7.TabIndex = 0;
            this.label7.Text = "使用科室：";
            // 
            // txtSyks
            // 
            this.txtSyks.Location = new System.Drawing.Point(141, 133);
            this.txtSyks.Name = "txtSyks";
            this.txtSyks.Size = new System.Drawing.Size(124, 26);
            this.txtSyks.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(290, 136);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 16);
            this.label8.TabIndex = 0;
            this.label8.Text = "供 货 商：";
            // 
            // txtGhs
            // 
            this.txtGhs.Location = new System.Drawing.Point(384, 136);
            this.txtGhs.Name = "txtGhs";
            this.txtGhs.Size = new System.Drawing.Size(124, 26);
            this.txtGhs.TabIndex = 8;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(47, 173);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(136, 16);
            this.label9.TabIndex = 0;
            this.label9.Text = "供货商联系方式：";
            // 
            // txtGhslxfs
            // 
            this.txtGhslxfs.Location = new System.Drawing.Point(175, 170);
            this.txtGhslxfs.Name = "txtGhslxfs";
            this.txtGhslxfs.Size = new System.Drawing.Size(333, 26);
            this.txtGhslxfs.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(61, 244);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(120, 16);
            this.label10.TabIndex = 0;
            this.label10.Text = "厂家联系方式：";
            // 
            // txtCj
            // 
            this.txtCj.Location = new System.Drawing.Point(175, 205);
            this.txtCj.Name = "txtCj";
            this.txtCj.Size = new System.Drawing.Size(333, 26);
            this.txtCj.TabIndex = 10;
            // 
            // txtCjlxfs
            // 
            this.txtCjlxfs.Location = new System.Drawing.Point(175, 241);
            this.txtCjlxfs.Name = "txtCjlxfs";
            this.txtCjlxfs.Size = new System.Drawing.Size(333, 26);
            this.txtCjlxfs.TabIndex = 11;
            // 
            // dtpYsrq
            // 
            this.dtpYsrq.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dtpYsrq.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpYsrq.Location = new System.Drawing.Point(141, 280);
            this.dtpYsrq.Margin = new System.Windows.Forms.Padding(4);
            this.dtpYsrq.Name = "dtpYsrq";
            this.dtpYsrq.Size = new System.Drawing.Size(124, 26);
            this.dtpYsrq.TabIndex = 13;
            // 
            // dtpRzrq
            // 
            this.dtpRzrq.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dtpRzrq.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpRzrq.Location = new System.Drawing.Point(385, 284);
            this.dtpRzrq.Margin = new System.Windows.Forms.Padding(4);
            this.dtpRzrq.Name = "dtpRzrq";
            this.dtpRzrq.Size = new System.Drawing.Size(124, 26);
            this.dtpRzrq.TabIndex = 15;
            // 
            // chkYsrq
            // 
            this.chkYsrq.AutoSize = true;
            this.chkYsrq.Location = new System.Drawing.Point(38, 283);
            this.chkYsrq.Name = "chkYsrq";
            this.chkYsrq.Size = new System.Drawing.Size(107, 20);
            this.chkYsrq.TabIndex = 12;
            this.chkYsrq.Text = "验收日期：";
            this.chkYsrq.UseVisualStyleBackColor = true;
            // 
            // chkRzrq
            // 
            this.chkRzrq.AutoSize = true;
            this.chkRzrq.Location = new System.Drawing.Point(275, 286);
            this.chkRzrq.Name = "chkRzrq";
            this.chkRzrq.Size = new System.Drawing.Size(107, 20);
            this.chkRzrq.TabIndex = 14;
            this.chkRzrq.Text = "入账日期：";
            this.chkRzrq.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(53, 317);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 16);
            this.label11.TabIndex = 12;
            this.label11.Text = "质 保 期：";
            // 
            // txtZbq
            // 
            this.txtZbq.Location = new System.Drawing.Point(141, 315);
            this.txtZbq.Name = "txtZbq";
            this.txtZbq.Size = new System.Drawing.Size(367, 26);
            this.txtZbq.TabIndex = 16;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(126, 208);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 16);
            this.label12.TabIndex = 13;
            this.label12.Text = "厂家：";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(53, 357);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(88, 16);
            this.label13.TabIndex = 12;
            this.label13.Text = "设备数量：";
            // 
            // txtSbsl
            // 
            this.txtSbsl.Location = new System.Drawing.Point(141, 354);
            this.txtSbsl.Name = "txtSbsl";
            this.txtSbsl.Size = new System.Drawing.Size(124, 26);
            this.txtSbsl.TabIndex = 17;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(275, 359);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(128, 16);
            this.label14.TabIndex = 12;
            this.label14.Text = "设备单价(万¥)：";
            // 
            // txtSbdj
            // 
            this.txtSbdj.Location = new System.Drawing.Point(391, 354);
            this.txtSbdj.Name = "txtSbdj";
            this.txtSbdj.Size = new System.Drawing.Size(117, 26);
            this.txtSbdj.TabIndex = 18;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(58, 393);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(88, 16);
            this.label15.TabIndex = 15;
            this.label15.Text = "备注说明：";
            // 
            // txtSbbzsm
            // 
            this.txtSbbzsm.Location = new System.Drawing.Point(141, 390);
            this.txtSbbzsm.Name = "txtSbbzsm";
            this.txtSbbzsm.Size = new System.Drawing.Size(367, 26);
            this.txtSbbzsm.TabIndex = 19;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(58, 431);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(88, 16);
            this.label16.TabIndex = 15;
            this.label16.Text = "设备类别：";
            // 
            // cmbSblb
            // 
            this.cmbSblb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSblb.FormattingEnabled = true;
            this.cmbSblb.Items.AddRange(new object[] {
            "普通",
            "计量",
            "特种",
            "射线装置"});
            this.cmbSblb.Location = new System.Drawing.Point(141, 428);
            this.cmbSblb.Name = "cmbSblb";
            this.cmbSblb.Size = new System.Drawing.Size(121, 24);
            this.cmbSblb.TabIndex = 20;
            // 
            // chkTf
            // 
            this.chkTf.AutoSize = true;
            this.chkTf.Location = new System.Drawing.Point(271, 430);
            this.chkTf.Name = "chkTf";
            this.chkTf.Size = new System.Drawing.Size(59, 20);
            this.chkTf.TabIndex = 21;
            this.chkTf.Text = "投放";
            this.chkTf.UseVisualStyleBackColor = true;
            // 
            // chkJk
            // 
            this.chkJk.AutoSize = true;
            this.chkJk.Location = new System.Drawing.Point(336, 430);
            this.chkJk.Name = "chkJk";
            this.chkJk.Size = new System.Drawing.Size(59, 20);
            this.chkJk.TabIndex = 23;
            this.chkJk.Text = "进口";
            this.chkJk.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(58, 462);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(120, 16);
            this.label17.TabIndex = 15;
            this.label17.Text = "外贸公司名称：";
            // 
            // txtWmgsmc
            // 
            this.txtWmgsmc.Location = new System.Drawing.Point(175, 458);
            this.txtWmgsmc.Name = "txtWmgsmc";
            this.txtWmgsmc.Size = new System.Drawing.Size(334, 26);
            this.txtWmgsmc.TabIndex = 25;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(58, 493);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(120, 16);
            this.label18.TabIndex = 15;
            this.label18.Text = "进口的报关单：";
            // 
            // txtBgd
            // 
            this.txtBgd.Location = new System.Drawing.Point(175, 489);
            this.txtBgd.Name = "txtBgd";
            this.txtBgd.Size = new System.Drawing.Size(334, 26);
            this.txtBgd.TabIndex = 26;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(58, 524);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(120, 16);
            this.label19.TabIndex = 15;
            this.label19.Text = "检验检疫证明：";
            // 
            // txtJyjyzm
            // 
            this.txtJyjyzm.Location = new System.Drawing.Point(175, 519);
            this.txtJyjyzm.Name = "txtJyjyzm";
            this.txtJyjyzm.Size = new System.Drawing.Size(334, 26);
            this.txtJyjyzm.TabIndex = 27;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(57, 557);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(120, 16);
            this.label20.TabIndex = 15;
            this.label20.Text = "下次检测日期：";
            // 
            // dtpjyrq
            // 
            this.dtpjyrq.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dtpjyrq.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpjyrq.Location = new System.Drawing.Point(175, 552);
            this.dtpjyrq.Margin = new System.Windows.Forms.Padding(4);
            this.dtpjyrq.Name = "dtpjyrq";
            this.dtpjyrq.Size = new System.Drawing.Size(124, 26);
            this.dtpjyrq.TabIndex = 28;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(61, 590);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(184, 16);
            this.label21.TabIndex = 15;
            this.label21.Text = "射线装置职业病预控评：";
            // 
            // textSxzz
            // 
            this.textSxzz.Location = new System.Drawing.Point(241, 585);
            this.textSxzz.Name = "textSxzz";
            this.textSxzz.Size = new System.Drawing.Size(257, 26);
            this.textSxzz.TabIndex = 29;
            // 
            // txtBfwh
            // 
            this.txtBfwh.Location = new System.Drawing.Point(241, 619);
            this.txtBfwh.Name = "txtBfwh";
            this.txtBfwh.Size = new System.Drawing.Size(257, 26);
            this.txtBfwh.TabIndex = 31;
            // 
            // chkBfwh
            // 
            this.chkBfwh.AutoSize = true;
            this.chkBfwh.Location = new System.Drawing.Point(60, 623);
            this.chkBfwh.Name = "chkBfwh";
            this.chkBfwh.Size = new System.Drawing.Size(187, 20);
            this.chkBfwh.TabIndex = 30;
            this.chkBfwh.Text = "报废设备的批准文号：";
            this.chkBfwh.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(61, 658);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(88, 16);
            this.label22.TabIndex = 15;
            this.label22.Text = "报废日期：";
            // 
            // dtpBfrq
            // 
            this.dtpBfrq.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dtpBfrq.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpBfrq.Location = new System.Drawing.Point(141, 652);
            this.dtpBfrq.Margin = new System.Windows.Forms.Padding(4);
            this.dtpBfrq.Name = "dtpBfrq";
            this.dtpBfrq.Size = new System.Drawing.Size(124, 26);
            this.dtpBfrq.TabIndex = 32;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(47, 9);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(88, 16);
            this.label23.TabIndex = 0;
            this.label23.Text = "合同编号：";
            // 
            // txtHtbh
            // 
            this.txtHtbh.Location = new System.Drawing.Point(141, 6);
            this.txtHtbh.Name = "txtHtbh";
            this.txtHtbh.ReadOnly = true;
            this.txtHtbh.Size = new System.Drawing.Size(367, 26);
            this.txtHtbh.TabIndex = 1;
            this.txtHtbh.TabStop = false;
            // 
            // button2
            // 
            this.button2.Image = global::Sbk_Manage.Properties.Resources.ok_16px_1134004_easyicon_net;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(298, 649);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(95, 38);
            this.button2.TabIndex = 33;
            this.button2.Text = "确认更新";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Frm_EditDevice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 691);
            this.Controls.Add(this.txtSbdj);
            this.Controls.Add(this.dtpjyrq);
            this.Controls.Add(this.dtpYsrq);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.dtpBfrq);
            this.Controls.Add(this.txtBfwh);
            this.Controls.Add(this.cmbSblb);
            this.Controls.Add(this.textSxzz);
            this.Controls.Add(this.txtJyjyzm);
            this.Controls.Add(this.txtBgd);
            this.Controls.Add(this.txtWmgsmc);
            this.Controls.Add(this.txtSbbzsm);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtSbsl);
            this.Controls.Add(this.txtCj);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.chkRzrq);
            this.Controls.Add(this.chkJk);
            this.Controls.Add(this.chkBfwh);
            this.Controls.Add(this.chkTf);
            this.Controls.Add(this.chkYsrq);
            this.Controls.Add(this.dtpRzrq);
            this.Controls.Add(this.txtGhs);
            this.Controls.Add(this.txtZczh);
            this.Controls.Add(this.txtSbtm);
            this.Controls.Add(this.txtXhgg);
            this.Controls.Add(this.txtZbq);
            this.Controls.Add(this.txtCjlxfs);
            this.Controls.Add(this.txtGhslxfs);
            this.Controls.Add(this.txtSyks);
            this.Controls.Add(this.txtSbpp);
            this.Controls.Add(this.txtCcbh);
            this.Controls.Add(this.txtHtbh);
            this.Controls.Add(this.txtSbmc);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_EditDevice";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "编辑设备";
            this.Load += new System.EventHandler(this.Frm_AddDevice_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSbmc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtXhgg;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCcbh;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSbtm;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtSbpp;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtZczh;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtSyks;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtGhs;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtGhslxfs;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtCj;
        private System.Windows.Forms.TextBox txtCjlxfs;
        internal System.Windows.Forms.DateTimePicker dtpYsrq;
        internal System.Windows.Forms.DateTimePicker dtpRzrq;
        private System.Windows.Forms.CheckBox chkYsrq;
        private System.Windows.Forms.CheckBox chkRzrq;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtZbq;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtSbsl;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtSbdj;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtSbbzsm;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cmbSblb;
        private System.Windows.Forms.CheckBox chkTf;
        private System.Windows.Forms.CheckBox chkJk;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtWmgsmc;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtBgd;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtJyjyzm;
        private System.Windows.Forms.Label label20;
        internal System.Windows.Forms.DateTimePicker dtpjyrq;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textSxzz;
        private System.Windows.Forms.TextBox txtBfwh;
        private System.Windows.Forms.CheckBox chkBfwh;
        private System.Windows.Forms.Label label22;
        internal System.Windows.Forms.DateTimePicker dtpBfrq;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtHtbh;
        private System.Windows.Forms.Button button2;
    }
}