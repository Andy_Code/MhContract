﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Globalization;
using Aspose.Words;

namespace Sbk_Manage
{
    public partial class Frm_Htjy : Form
    {
        public Frm_Htjy()
        {
            InitializeComponent();
            this.KeyPreview = true;
            //修改日期格式
            Thread.CurrentThread.CurrentCulture = new CultureInfo("zh-CN");
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Thread.CurrentThread.CurrentCulture.Clone();
            Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern = "yyyy-MM-dd HH:mm:ss";
        }
        //打印借阅单  
        private void PrintJyd()
        {
            try
            {
                //模板路径
                string JydPath = Program.APPdirPath + @"\合同借阅单A4.dot";
                if (System.IO.File.Exists(JydPath) == false)
                {
                    MessageBox.Show("合同借阅单A4.dot不存在!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                Document doc = new Document(JydPath);
                if (doc.Range.Bookmarks["合同名称"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["合同名称"];
                    mark.Text = txtHtmc.Text.Trim();
                }
                if (doc.Range.Bookmarks["合同编号"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["合同编号"];
                    mark.Text = txtHtbh.Text.Trim();
                }
                if (doc.Range.Bookmarks["借阅人"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["借阅人"];
                    mark.Text = txtJyr.Text.Trim();
                }
                if (doc.Range.Bookmarks["借阅时间"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["借阅时间"];
                    mark.Text = dtpHtJcrq.Value.ToString("yyyy-MM-dd HH:mm:ss");
                }
                if (doc.Range.Bookmarks["合同名称1"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["合同名称1"];
                    mark.Text = txtHtmc.Text.Trim();
                }
                if (doc.Range.Bookmarks["合同编号1"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["合同编号1"];
                    mark.Text = txtHtbh.Text.Trim();
                }
                if (doc.Range.Bookmarks["借阅人1"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["借阅人1"];
                    mark.Text = txtJyr.Text.Trim();
                }
                if (doc.Range.Bookmarks["借阅时间1"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["借阅时间1"];
                    mark.Text = dtpHtJcrq.Value.ToString("yyyy-MM-dd HH:mm:ss");
                }
                //打印
                PrintDialog printDlg = new PrintDialog();
                printDlg.AllowSomePages = true;
                printDlg.PrinterSettings.MinimumPage = 1;
                printDlg.PrinterSettings.MaximumPage = doc.PageCount;
                printDlg.PrinterSettings.FromPage = 1;
                printDlg.PrinterSettings.ToPage = doc.PageCount;
                printDlg.PrinterSettings.Copies = 1;
                doc.Print(printDlg.PrinterSettings, txtHtmc.Text.Trim());
            }
            catch
            {

            }
        }
        //主键
        public Int32 CurHtID = 0;
        //合同编号
        public string CurHtbh = "";
        //合同名称
        public string CurHtmc = "";
        private void Frm_Htjy_Load(object sender, EventArgs e)
        {
            dtpHtJcrq.Value = DateTime.Now;
            txtHtmc.Text = CurHtmc;
            txtHtbh.Text = CurHtbh;
            //查询是否有借阅信息
            string sqlstr = "select jyr,strftime('%Y-%m-%d %H:%M:%S',jysj) as jysj from htjy_info  where ghsj is null and htid=" + CurHtID;
            DataTable dt = Program.SqliteDB.GetDataTable(sqlstr);
            if (dt != null && dt.Rows.Count == 1)
            {
                button1.Enabled = false;
                button2.Enabled = true;
                button3.Enabled = true;
                txtJyr.Text = dt.Rows[0]["jyr"].ToString();
                txtJyr.ReadOnly = true;
                dtpHtJcrq.Value = Convert.ToDateTime(dt.Rows[0]["jysj"].ToString());
                dtpHtJcrq.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
                button2.Enabled = false;
                button3.Enabled = false;
            }
        }
        //归还
        private void button2_Click(object sender, EventArgs e)
        {
            string sqlstr = "update htjy_info set ghsj='" + DateTime.Now.ToString("s") + "' where ghsj is null and htid=" + CurHtID;
            if (Program.SqliteDB.ExecuteNonQuery(sqlstr) == 1)
            {
                sqlstr = "update ht_info set jc_flag=0 where id=" + CurHtID;
                if (Program.SqliteDB.ExecuteNonQuery(sqlstr) == 1)
                {
                    MessageBox.Show("合同已归还!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    DialogResult = DialogResult.OK;
                }
            }
        }
        //借阅并打印
        private void button1_Click(object sender, EventArgs e)
        {
            if (txtJyr.Text.Trim().Equals(""))
            {
                MessageBox.Show("借出人不能为空!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtJyr.Focus();
                return;
            }
            button1.Enabled = false;
            string sqlstr = string.Format("insert into htjy_info values(NULL,{0},'{1}','{2}',NULL)",CurHtID,txtJyr.Text.Trim(),dtpHtJcrq.Value.ToString("s"));
            if (Program.SqliteDB.ExecuteNonQuery(sqlstr) == 1)
            {
                sqlstr = "update ht_info set jc_flag=1 where id=" + CurHtID;
                if (Program.SqliteDB.ExecuteNonQuery(sqlstr) == 1)
                {
                    //打印
                    PrintJyd();
                    MessageBox.Show("合同已借出!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    DialogResult = DialogResult.OK;
                }
            }
        }
        //借阅单打印
        private void button3_Click(object sender, EventArgs e)
        {
            button3.Enabled = false;
            Application.DoEvents();
            PrintJyd();
            button3.Enabled = true;
        }
    }
}
